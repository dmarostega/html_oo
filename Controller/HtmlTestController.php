<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 04/11/2013
 */

 include 'Model/HtmlTest.class.php';
 
function __autoload($classe){
	//include "Model/{$classe}.php";
}
class HtmlTestController
{	
	private $name = 'HtmlTest';
	private $htmlTest;
	
	public function __constructor(){
		$this->$htmlTest = new HtmlTest();		
	}
	
	public function getMenu(){
		return $this->htmlTest->getMenu();
	}	
}
