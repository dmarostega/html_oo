<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 01/10/2013
 */

class TPanel extends TElement{
	
	private $style;
	private $name;

	public function __construct($name,$stype = 1){
		$this->name = $name;
		$this->setStyle($stype);		
		parent::__construct('div');
	}
	
	public function getStyle(){
		return $this->style;
	}
	
	/*
	 * Style Type
	* $stype  = type item
	* 			1 = class (default)
	* 			2 = id
	* */
	public function setStyle($stype = null){	
		$this->style = new TStyleItem($this->name,$stype);
		switch ($stype){
			case 1 :
				default:
				$this->class = $this->name;
				break;
			case 2 :
				$this->id = $this->name;
				break;				
		}		
	}
		
	public function put($widget,$col,$row){
		$camada = new TElement('div');
		$camada->style = "position: absolute; left:{$col}" .(is_string($col) ? '': 'px')."; top:{$row}" .(is_string($row) ? '': 'px').";";
		$camada->add($widget);
		
		parent::add($camada);
	}	
}