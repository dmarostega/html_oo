<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 03/10/2013
 * @version: 1.0
 * @uses: Component for TStyle Class
 */

class TStyleItem {

	private $name;
	private $properties;
	private $type;
	/*
	 * Constructor
	* $name = name of item
	* $type  = type item
	* 			1 = class (default)
	* 			2 = id
	* */

	public function __construct($name, $type = 1){
		$this->name = $name;
		$this->setType($type);
	}

	public function  __set($name, $value){
		$name = str_replace('_','-',$name);
		$this->properties[$name] = $value;
	}
	public function show(){
		echo $this->getType().$this->name;
		echo "\n{";
		foreach ($this->properties as $name => $value){
			echo "\t {$name}: {$value};\n";
		}
		echo "}\n";
	}

	private function getType(){
		return $this->type;
	}

	private function setType($type){
		switch ($type){
			case 1:
			default:
				$type = '.';
				break;
			case 2:
				$type = '#';
				break;
		}
		$this->type = $type;
	}

	public function getName(){
		return $this->name;
	}
}