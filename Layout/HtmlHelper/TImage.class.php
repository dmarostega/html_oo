<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 01/10/2013
 */

class TImage extends TElement{
	
	function __construct($source){
		parent::__construct('img');
		$this->src = $source;
		$this->border = 1;
	}
	
}