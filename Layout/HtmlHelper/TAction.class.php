<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 09/10/2013
 */

class TAction{
	private $action;
	private $param;
	
	public function __construct($action){
		$this->action =  $action;		
	}
	
	public function setParameter($param, $value){
		$this->param[$param] = $value;
	}
	
	/*
	 * transforms the action into a string of type URL 
	 */
	
	public function serialize(){
		//check if the action is a method
		if (is_array($this->action)){			
			$url['class'] = get_class($this->action[0]);
			$url['method'] = $this->action[1];
		}
		else if (is_string($this->action)){
			$url['method'] = $this->action;
		}
		
		if ($this->param){
			$url = array_merge($url,$this->param);
		}
		
		//monta url
		return '?' . http_build_query($url);		
	}
}