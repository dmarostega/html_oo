<?php
/**
 * @author: Diogo Marostega de Oliveira
 * @date: 01/10/2013
 */

class TStyle{
	private $name;
	static private $properties;
	static private $loaded;
	private $media;
	
	private static $itens;
	private static $styles;
	
    public function __construct($media = 'screen'){	
		$this->media =  $media;
	}
	
	public function setItem($item){
		switch ($item){
			case is_object($item):
				$this->setItemObject($item);
			break;
			case is_array($item):
				$this->setItemArr($item);
			break;
			default:
				$this->setItemNoObject($item);
			break;
		}			
	}
	
	private function setItemArr($item){
		foreach ($item as $name => $value){
			$this->setItens($value);							
		}		
	}
	
	private function setItemObject($value){
		$this->setItens($value);		
	}
	
	private function setItemNoObject(){
		$this->setItem(new TStyleItem($item));
	}
	
	public function getItens(){
		return $this->itens;
	}
	
	private function setItens($item){
		$this->itens[] = $item; 
	}
	
	public function __set($name, $value){
		$name = str_replace('_','-',$name);
	}

	private function open(){
		echo "<style type='text/css' media='" . $this->media ."'>\n";
	}
	
	private function close(){
		echo "</style>\n";
	}
	
	public function show(){		

		$this->open();
		if ($this->itens){
			foreach ( $this->itens as $item){
				$item->show();
			}
		}
			$this->close();
		if (!self::$loaded[$this->name]){
 			self::$loaded[$this->name] = TRUE; 
		} 
	}
}