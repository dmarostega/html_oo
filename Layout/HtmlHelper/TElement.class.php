<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 01/10/2013
 */
class TElement {
	
    private $name;
	private $properties;

	function __construct($name){
		$this->name = $name;		
	}	
	
	//tag de abertura. Defini��o de propriedades
	function __set($name,$value){
		$this->properties[$name] = $value;
	}
	
	//Adiciona conteudo
	function add($child){
		if (is_array($child)){
			foreach ($child as $key => $value){
				$this->children[] = $value;
			}
		}else{
			$this->children[] = $child;
		}
	}
	
	//Define tag abertura
	function open(){
		echo "\n<{$this->name} ";
		if ($this->properties){
			foreach ($this->properties as $name => $value){
				echo "{$name}=\"{$value}\" ";
			}
		}
		//Final tag abertura
		echo ">";
	}
	
	public function show(){
		$this->open();
		//echo "\n";
		
		if ($this->children){
			foreach ($this->children as $child){
			if (is_object($child)){
				$child->show();
			}
			else if ((is_string($child)) or (is_numeric($child)))
			{
				echo $child;
			}
		}
	
		}
		//fecha tag
	$this->close();
	}
	
	private function close(){
		echo "</{$this->name}> \t\t<!-- END {$this->name} {$this->properties['class']} TAG -->\n";
		
	}
	
	function __toString(){
		return $this->name;
	}

	
}