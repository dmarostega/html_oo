<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 06/11/2013
 */

class TWindow
{
	private $x;
	private $y;
	private $width;
	private $height;
	private $title;
	private $content;
	static private $counter;
	
	public function __construct(){
		sefl::$counter++;
		$this->title = $title;
	}
	
	/**
	 * Metodo setPosition
	 * define a coluna e linha (x,y) que a janela ser� exibida na tela
	 * $x = coluna (px)
	 * $y = linha (px)
	 * */
	public function setPosition($x,$y){
		$this->x = $x;
		$this->y = $y;
	}
	
	/**
	 * Metodo setSize
	 * define a largura e altura da janela
	 * @param $width = largura (px)
	 * @param $height = altura (px)
	 * */
	public function setSize($width, $height){
		$this->width = $width;
		$this->height = $height;
	}
	
	/**
	 * Metodo Add
	 * adiciona conteudo � janela
	 * @param $content = conte�do a ser adicionado
	 * */
	public function add($content){
		$this->content = $content;
	}
}