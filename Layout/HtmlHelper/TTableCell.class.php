<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 01/10/2013
 */

class TTableCell extends TElement{
	
	public function __construct($value,$t = 'td'){
		parent::__construct($t);
		parent::add($value);
	}
}