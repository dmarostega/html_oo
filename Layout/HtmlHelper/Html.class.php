<?php
/*
 * @Autor: Diogo Marostega de Oliveira
* @Data: 26/09/2013
*/

class Html extends TElement
{
	private $head;
	private $body;
	private $style;
	private $title;
	

	function __construct(){		
		parent::__construct('html');
		$this->head = new TElement('head');
		$this->body = new TElement('body');
		$this->add($this->head);
		$this->add($this->body);
	}
	
	public  function setTitle($title){
		$this->title = new TElement('title');
		$this->title->add($title);
		$this->setHead($this->title);
	}
	
	public function getHead(){
		return $this->head;
	}
	
	public function getBody(){
		return $this->body;
	}
	
	public function setHead($item){
		$this->head->add($item); 
	}
	
	public function setBody($item){
		if (is_array($item)){
			foreach ($item as $name => $value){
				$this->body->add($value);
			}
		}else{ 
			$this->body->add($item);
		}
	}
	
	public function run(){
		if($_GET){			
			$class = $_GET['class'];
			$method = $_GET['method'];				
			if($class)
			{
				$object = $class == get_class($this) ? $this : new $class;
				if (method_exists($object, $method)){
					call_user_func(array($object, $method), $_GET);
				}
			}
			else if (function_exists($method)){
				call_user_func($method, $_GET);
			}
		}
	}
	
	public function show(){
		$this->run();
		parent::show();
	}
}
/* 	
 * DEPRECATED
 * 
 * 
	function setBody($body){
		$this->body = $body;		
	}
	
	function getBody(){
		return $this->body;
	}
	
	function setHead($head){
		$this->head = $head;
	} */
// 	function arrHtml(){				
// 		//$arrCode = array();
// 		return $arrcode = array('html' =>  
// 								 array('head' => array('title' => $this->getTitle(),
// 								 					   'link' => 'cass.sss'), 
// 								 	   'body' => 'p' 
// 								 		)
// 							);
 	
// 	}

// 	function foreachRecursive($itens){

// 		foreach ($itens as $key => $value){
// 			$this->tags .= '<' . $key . '>';
// 			if (is_array($value)){
// 				$this->foreachRecursive($value);
// 			}else{
// 				 $this->tags .= $value;				
// 			}
// 			 $this->tags .= '</' . $key . '>';			
// 		}	
// 		return	$this->tags;
// 	}
// }	