<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 06/11/2013
 */

class TParagraph extends TElement
{
	/*
	 * Metodo Construtor
	 * */

	public function __construct($text){
		parent::__construct('p');
		parent::add($text);
	}
	
	public function setAlign($align){
		$this->align = $align;
	}
}
