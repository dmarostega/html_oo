<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 01/10/2013
 */

class TTableRow extends TElement{
	
	public function __construct(){
		parent::__construct('tr');
	}
	
	public function addCell($value,$t = 'td'){
		$cell = new TTableCell($value,$t);
		parent::add($cell);
		return $cell;
	}
}