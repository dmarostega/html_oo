<?php
/**
 * @Author: Diogo Marostega de Oliveira
 * @Date: 12/10/2013
 */

 function __autoload($classe){
 	include_once "Layout/HtmlHelper/{$classe}.class.php";
} 

include_once "Model/HtmlTest.class.php";
 include 'Config/config.php';
// include 'connection.php';

$html = new Html();
$html->setTitle(getName());
$linkCSS = new TElement('link');
$linkCSS->rel = 'stylesheet';
$linkCSS->type = 'text/html';
$linkCSS->href = 'css/style.css';

$html->setHead('<link rel="stylesheet" type="text/css" href="Layout/css/style.css">');

$divTop = new TPanel('dTop',2);
$divTopTitle = new TPanel('dTopTitle',2);
$divMenu = new TPanel('dMenu');
$divContent = new TPanel('dContent'); 
$style = new TStyle();

	$stlTop = $divTop->getStyle();
	$stlTop->width = auto;
	$stlTop->height = '10%';
	//$stlTop->border = '1px solid blue';
	$stlTop->position = 'relative';
	$stlTop->background_color = 'black';
	
	$stlTopTitle = $divTopTitle->getStyle();
	$stlTopTitle->position = 'relative';
	$stlTopTitle->left = '5%';
	$stlTopTitle->top = '25%';
	$stlTopTitle->color = 'white';
	$stlTopTitle->font_size = 29;
	
		$title = new TElement('p');
		$title->add(getName());		
		$divTopTitle->add($title);
	
	$divTop->add($divTopTitle);

	$stlMenu = $divMenu->getStyle();
	$stlMenu->background_color = 'gray';

	
	//foreach (getMenu() as $key => $value){
	//	echo "$key: $value<BR>";
		$menu = new TElement('a');
		$menu->class = 'menuItem';
	$test = new HtmlTest();
		//$action = new TAction(array($test,"action"));
		//$menu->href = $action->serialize(); 
		$menu->add("Testando");
		$divMenu->add($menu);
//	}	
	
	$stlContent = $divContent->getStyle();
	$stlContent->width = '100%';
	$stlContent->height = 'auto';
	$stlContent->display = 'table';
	$stlContent->margin = '0 auto';

	$html->setHead($style);
	
	$style->setItem(array($stlTop,$stlTopTitle,$stlMenu, $stlContent));

	$html->setBody(array($divTop,$divMenu,$divContent)) ;
	$html->show();

/* 	
 * DEPRECATED
 * 
 * function getDiv($divs){
	 	static $k;
	 	
		if (is_array($divs)){
			foreach ($divs as $key => $value){
				if (is_numeric($key)){
					$this->getDiv($value);
				}else{
					$k = $value;
					$this->getDiv($key);
				}			
			}		
		}else{
			$div[] = new TPanel($divs,$k);	
			$k = null;
		}
		return $div;
	} */

