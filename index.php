<?php
/**
 * @Autor: Diogo Marostega de Oliveira
 * @Data: 26/09/2013
 * @Modified: 05/11/2013
 */

include_once "Model/HtmlTest.class.php";

include_once "Layout/Layout.php";

class TAplication
{	
	static public function run()
	{
		if ($_GET){
			$class = $_GET['class'];
			
			if (class_exists($class)){
				$pagina = new $class;
				ob_start();
				$pagina->show();
				$content = ob_get_contents();
				ob_end_clean();
			}
			else if (function_exists($method))
			{
				call_user_func($method,$_GET);
			}
		}		
		echo str_replace('#CONTENT#',$content, $template);
	}
}
TAplication::run();
